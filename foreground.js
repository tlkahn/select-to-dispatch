// This script gets injected into any opened page
// whose URL matches the pattern defined in the manifest
// (see "content_script" key).
// Several foreground scripts can be declared
// and injected into the same or different pages.

console.log('This is the foreground.js running...');

async function copyTitleAndUrl() {
  const title = document.title;
  const url = window.location.href;
  const text = `[[${url}][${title}]]`;
  await navigator.clipboard.writeText(text);
}

document.addEventListener('keydown', async (e) => {
  if (e.ctrlKey && e.code === 'KeyY') {
    await copyTitleAndUrl();
  }
});
