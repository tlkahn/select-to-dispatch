console.log(
  'This prints to the console of the service worker (background script)'
);

function handleCommand(handler) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    handler(tabs[0]);
  });
}

chrome.commands.onCommand.addListener((command) => {
  if (command === 'run') handleCommand(executeScript);
  if (command === 'custom_browser_action')
    handleCommand(execute_browser_action);
});
function execute_browser_action(tab) {
  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    files: ['browser_action.js'],
  });
}

function executeScript(tab) {
  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    files: ['capture.js'],
  });
}

chrome.action.onClicked.addListener((tab) => {
  executeScript(tab);
});

chrome.runtime.onMessage.addListener((message, _, sendResponse) => {
  const url = `http://localhost:8888/`;
  const formData = new FormData();
  Object.entries(message).forEach(([key, value]) =>
    formData.append(key, value)
  );
  fetch(url, { method: 'POST', body: formData })
    .then((res) => sendResponse({ status: 'ok', res }))
    .catch((err) => sendResponse({ status: 'error', err }));
  return true;
});
