(() => {
  const IFRAME_FIXEES = ['medium.com', 'towardsdatascience.com'];
  const URL = `http://localhost:7000/`;

  function stripInvalidFileNameChars(s) {
    return s
      .replace(/[/\\?%*:|"<>~]/g, '-')
      .replace(
        /([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g,
        ''
      );
  }

  function cleanIFrames(domElem) {
    return new Promise((resolve, _) => {
      const iframes = domElem.querySelectorAll('iframe');
      const ps = [];
      for (const iframe of iframes) {
        const div = document.createElement('div');
        const src = iframe.src;
        ps.push(
          fetch(src).then(async (res) => {
            div.innerHTML = await res.text();
            const scripts = div.querySelectorAll('script');
            for (const script of scripts) {
              if (script.src) {
                const newScriptTags = document.createElement('div');
                newScriptTags.setAttribute('gistLink', script.src);
                iframe.parentNode.appendChild(newScriptTags);
              }
            }
          })
        );
      }
      Promise.all(ps).then(() => {
        resolve(domElem);
      });
    });
  }

  const capture = (iframe_fixees, server_url) => {
    console.log('This is the capture.js running...');
    const sel = window.getSelection();
    let html = '';
    if (typeof sel != 'undefined') {
      if (sel.rangeCount) {
        const container = document.createElement('div');
        for (let i = 0, len = sel.rangeCount; i < len; ++i) {
          container.appendChild(sel.getRangeAt(i).cloneContents());
        }
        html = container.innerHTML;
      }
    } else if (typeof document.selection != 'undefined') {
      // For IE < 9, you should not use this
      if (document.selection.type == 'Text') {
        html = document.selection.createRange().htmlText;
      }
    }
    const relToAbs = (href) => {
      const a = document.createElement('a');
      a.href = href;
      const abs = a.protocol + '//' + a.host + a.pathname + a.search + a.hash;
      a.remove();
      return abs;
    };
    const elementTypes = [
      ['a', 'href'],
      ['img', 'src'],
    ];
    const div = document.createElement('div');
    div.innerHTML = html;
    elementTypes.map((elementType) => {
      const elements = div.getElementsByTagName(elementType[0]);
      for (let i = 0; i < elements.length; i++) {
        elements[i].setAttribute(
          elementType[1],
          relToAbs(elements[i].getAttribute(elementType[1]))
        );
      }
    });
    let selection_html = '';
    const href = location.href;
    selection_html =
      div.innerHTML + `<hr/><div class='source-url'>${href}</div>`;
    const title = document.title;
    const filename_main = title || href;
    const suffix = 'org';
    const filename = stripInvalidFileNameChars(filename_main + `.${suffix}`);
    if (!filename) {
      filename = 'untitled.md';
    }
    chrome.runtime.sendMessage(
      chrome.runtime.id,
      {
        selection_html,
        url: href,
        title,
        filename,
        server_url,
      },
      null,
      console.log
    );
  };

  capture(IFRAME_FIXEES, URL);
})();
